package com.appleyk;

import com.appleyk.model.User;
import org.springframework.stereotype.Service;

/**
 * <p>越努力，越幸运</p>
 *
 * @author appleyk
 * @version V.0.1.1
 * @blob https://blog.csdn.net/appleyk
 * @github https://github.com/kobeyk
 * @date created on  下午9:35 2022/11/23
 */
@Service
public class MyPay implements IPay{
    @Override
    public String pay() {
        User user = new User("appleyk","18");
        System.out.println("自定义的插件实现支付接口 -- v3！");
        return "自定义的插件实现支付接口 -- v3！";
    }
}
